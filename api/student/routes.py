import datetime
from flask_openapi3 import APIBlueprint
from pydantic import BaseModel
from sqlalchemy import select
from api.student.models import Student
from database import db
from flask import jsonify, request

students_app = APIBlueprint("strudent_app", __name__)

class StudentSchema(BaseModel):
    id: int
    enrollment_date: datetime.datetime
    min_course_credits: int
    first_name: str
    last_name: str
    user_id: int

class StudentsList(BaseModel):
    students: list[StudentSchema]

@students_app.post("/students")
def post_student():
    data = request.json
    enrollment_date = datetime.datetime()
    min_course_credits = data.get("minCourseCredits")
    first_name = data.get("firstName")
    last_name = data.get("lastName")
    user_id = data.get("userId")

    with db.session() as session:
        student = Student(id=id, 
                           first_name= first_name, 
                           last_name=last_name, 
                           enrollment_date= enrollment_date,
                           min_course_credits= min_course_credits,
                           user_id = user_id
                           )
        
        new_student = session.insert(student)
        return f"/students/{new_student.id}"
         
@students_app.get("/students", responses={"200": StudentsList})
def get_students():
    with db.session as session:
        students = session.execute(select(Student)).scalar().all()
        return students

@students_app.get("/students/:<id>", responses={"200": StudentSchema})
def get_students():

    data = request.json
    id = data.get("id")

    with db.session as session:
        student = session.execute(select(Student).where(Student.id == id))
        return student
